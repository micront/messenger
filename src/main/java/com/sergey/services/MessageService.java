package com.sergey.services;


import com.sergey.model.Message;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface MessageService {

    void saveNewMessage(Message message);
}
