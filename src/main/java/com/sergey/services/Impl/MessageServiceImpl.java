package com.sergey.services.Impl;


import com.sergey.model.Message;
import com.sergey.repositories.MessageRepository;
import com.sergey.services.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageRepository repo;

    @Override
    public void saveNewMessage(Message message) {
        repo.saveMessage(message);
    }

}
