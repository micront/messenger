package com.sergey.model.JSON;

import lombok.Data;

@Data
public class MessageJSON {

    private String message;
    private String author;
}
