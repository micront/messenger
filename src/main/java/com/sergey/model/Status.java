package com.sergey.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_status")
    private long statusId;

    @Column
    private String text;

    @OneToMany(mappedBy = "status")
    private Set<Message> messages;
}
