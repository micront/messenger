package com.sergey.controller;

import com.sergey.model.JSON.MessageJSON;
import com.sergey.model.Message;
import com.sergey.model.Status;
import com.sergey.services.Impl.MessageServiceImpl;
//import com.sergey.services.MessageService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

//	private Logger log = LoggerFactory.getLogger(HelloController.class);

	@Autowired
	private MessageServiceImpl messageService;



	@GetMapping("/")
	public ModelAndView hello() {
		ModelAndView model = new ModelAndView("welcome");
		model.addObject("name","Sergey");
//		model.addAttribute("name", "John Doe");
		messageService.saveNewMessage(new Message("Sergey",new Status()));
		return model;
	}

	@PostMapping("/sendMessage")
	public String sendMessage(MessageJSON message){
//		log.info("in send message");
		System.out.println(message.getMessage());

		return "hi";
	}
}
