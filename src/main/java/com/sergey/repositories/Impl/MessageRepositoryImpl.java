package com.sergey.repositories.Impl;

import com.sergey.model.Message;
import com.sergey.repositories.AbstractRepository;
import com.sergey.repositories.MessageRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class MessageRepositoryImpl extends AbstractRepository implements MessageRepository {


    @Override
    public void saveMessage(Message message) {
        persist(message);
    }

}
