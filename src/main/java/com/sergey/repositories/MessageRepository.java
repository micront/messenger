package com.sergey.repositories;


import com.sergey.model.Message;


public interface MessageRepository {

    void saveMessage(Message message);

}
